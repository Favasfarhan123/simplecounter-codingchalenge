const h1=document.getElementById('h1')
const addButton=document.getElementById('addButton')
const minusButton=document.getElementById('minusButton')
const resetButton=document.getElementById('resetButton')
let number=0

addButton.addEventListener('click',add)

function add(){
    
    number=number+1
   
    h1.innerHTML=number
}
minusButton.addEventListener('click',minus)

function minus(){
    number=number-1
    h1.innerHTML=number
}

resetButton.addEventListener('click',reset)

function reset(){
    number=0
    h1.innerHTML=number
}